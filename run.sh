#!/bin/bash
IMAGE="go-zeromq"
TAG="latest"

docker rm -f $IMAGE

docker run -tid --restart=always \
-e GIT_REPOSITORY=https://wiraperdana@bitbucket.org/ramprasath_balakrishnan/hello_world.git \
-p 19081:8080 \
--name $IMAGE \
anilornd/$IMAGE:$TAG
