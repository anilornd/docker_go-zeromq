#!/bin/sh

mkdir logs

cd /go/src

# git clone on deploy
if [ ! -d app ] ; then
  echo "cloning app repository..."
  git clone ${GIT_REPOSITORY} app
  cd app
  echo "installing dependencies..."
  ls -al
  go get
  go build 
  echo "running go app..."
  ./app
fi
